package com.kurs;

public class App {
    public static void main(String args[]) {

        if (args.length != 3) {

            System.err.println("Invalid arguments number");
            System.exit(1);
        }
        float x = Float.valueOf(args[0]);
        float y = Float.valueOf(args[1]);
        String z = args[2];
        calculate(x, y, z);
    }

    private static void calculate(float x, float y, String z) {
        float result;
        switch (z) {
            case "+":
                result = x + y;
                break;
            case "-":
                result = x - y;
                break;
            default:
                throw new IllegalArgumentException("Unsupported operator: " + z);
        }
        System.out.println("number1=" + x + " number2=" + y + " operator=" + z + " result=" + result);//todo improve
    }


}
