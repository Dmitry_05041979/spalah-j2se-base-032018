package com.collection;


public interface MyList extends MyCollection {
    void add(int index, Integer x);

    Object get(int index);

    int indexOf(Integer x);

    Object remove(int index);

    void trimToSize();
}