package com.collection;

/**
 * Created by admin on 14.04.2018.
 */
public interface MyCollection {

    void add(Integer value);

    void clear();

    int size();

    boolean isEmpty();

    boolean contains(Integer x);

    Integer[] toArray();
}