package com.collection;

import java.util.Arrays;

public class MyArrayList implements MyList {
    private static final int INIT_SIZE = 5;
    private int size;
    private Integer[] arr;

    public MyArrayList() {
        this.arr = new Integer[INIT_SIZE];

    }

    @Override
    public void add(Integer value) {
        MyCollection myCollection = new MyArrayList();

        if (size >= arr.length) {
            increaseArraySize();

        }
        arr[size] = value;
        size++;
    }

    private void increaseArraySize() {
        arr = Arrays.copyOf(arr, arr.length * 2);
    }


    @Override
    public void clear() {
        size = 0;
       /* MyCollection myCollection = new MyArrayList();
        myCollection.clear();*/

    }

    @Override
    public int size() {
     /*   MyCollection myCollection = new MyArrayList();
        int size = myCollection.size();
        System.out.println("Size of list = " + size);*/
        return size;
    }

    @Override
    public boolean isEmpty() {
        /*MyCollection myCollection = new MyArrayList();
        boolean retval = myCollection.isEmpty();
        if (retval == true) {
            System.out.println("list is empty");
        } else {
            System.out.println("list is not empty");
        }

        for (Integer number : arrlist) {
            System.out.println("Number = " + number);
        }*/

        return size == 0;
    }

    @Override
    public boolean contains(Integer x) {
       /* MyCollection myCollection = new MyArrayList();
        boolean retval = myCollection.contains(x);

        if (retval == true) {
            System.out.println("element 10 is contained in the list");
        } else {
            System.out.println("element 10 is not contained in the list");
        }*/
        return false;
    }

    @Override
    public Integer[] toArray() {

        /*MyCollection myCollection = new MyArrayList();
        Integer list2[] = new Integer[myCollection.size()];
        list2 = myCollection.toArray(list2);

        System.out.println("Printing elements of array2");

        for (Integer number : list2) {
            System.out.println("Number = " + number);*/

        return new Integer[0];
    }

    @Override
    public void add(int index, Integer x) {
           /* MyCollection myCollection = new MyArrayList();
            myCollection.add(index,x);

             for (Integer number : myCollection) {
                System.out.println("Number = " + number);*/

    }

    @Override
    public Object get(int index) {
          /*  MyCollection myCollection = new MyArrayList();
            int retval = myCollection.get(3);
            System.out.println("Retrieved element is = " + retval);*/
        return null;
    }

    @Override
    public int indexOf(Integer x) {
          /*  MyCollection myCollection = new MyArrayList();
            int retval = myCollection.indexOf("2");
            System.out.println("The element E is at index " + retval);
            */
        return 0;
    }

    @Override
    public Object remove(int index) {
           /* MyCollection myCollection = new MyArrayList();
            myCollection.remove(2);
            System.out.println("ArrayList After remove:");
            for (Integer var2: myCollection){
                System.out.println(var2);*/

        return null;
    }

    @Override
    public void trimToSize() {
       /* MyCollection myCollection = new MyArrayList();
        myCollection.trimToSize();
        for (Integer number : myCollection) {
            System.out.println("Number = " + number);
*/
        }
    }




