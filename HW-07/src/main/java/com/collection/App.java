package com.collection;

public class App {
    public static void main(String[] args) {
        MyArrayList list = new MyArrayList();

        int a=2, b=20;

        list.add(a);
        list.clear();
        list.size();
        list.isEmpty();
        list.contains(a);
        list.add(a, b);
        list.get(a);
        list.indexOf(a);
        list.remove(a);
        list.trimToSize();
    }
}
