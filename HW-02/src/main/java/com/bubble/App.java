package com.bubble;

import java.util.Random;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        int row = 8, column = 5;
        int[][] arr = new int[row][column];
        Random random = new Random();
        int min = -50;
        int max = 100;


        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                arr[i][j] = random.nextInt(max - min + 1) + min;
            }
        }
        for (int a = 0; a < row; a++) {
            for (int b = 0; b < column; b++) {
                System.out.print("|" + "\t" + arr[a][b] + "\t" + "|");
            }
            System.out.println();
        }
        int max_value = arr[0][0];
        for (int c = 0; c < row; c++) {
            for (int d = 0; d < column; d++) {
                if (max_value <= arr[c][d]) {
                    max_value = arr[c][d];
                }
            }
        }
        System.out.println("max value:" + max_value);

        int min_value = arr[0][0];
        for (int c = 0; c < row; c++) {
            for (int d = 0; d < column; d++) {
                if (min_value >= arr[c][d]) {
                    min_value = arr[c][d];
                }
            }
        }
        System.out.println("min value:" + min_value);


        int max_product_row = 0;
        int index_row = 0;
        for (int c = 0; c < row; c++) {
            int product = 1;
            for (int d = 0; d < column; d++) {
                product *= arr[c][d];
            }
            if (Math.abs(product) > max_product_row) {
                max_product_row = product;
                index_row = c;
            }
        }
        System.out.println("maximum product of values (modulo) in a row:" + max_product_row + "\t" + "index of a row:" + index_row);

        int max_product_column = 0;
        int index_column = 0;
        for (int c = 0; c < column; c++) {
            int product = 1;
            for (int d = 0; d < row; d++) {
                //int product = 1;

                product *= arr[d][c];
            }
            if (Math.abs(product) > max_product_column) {
                max_product_column = product;
                index_column = c;
            }
        }
        System.out.println("maximum product of values (modulo) in a column:" + max_product_column + "\t" + "index of a column:" + index_column);


        for (int a = 0; a < row; a++) {
            for (int i = column - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (arr[a][j] > arr[a][j + 1]) {
                        int t = arr[a][j];
                        arr[a][j] = arr[a][j + 1];
                        arr[a][j + 1] = t;
                    }
                }
            }
        }

        for (int x = 0; x < row; x++) {
            for (int y = 0; y < column; y++) {
                System.out.print("|" + "\t" + arr[x][y] + "\t" + "|");
            }
            System.out.println();
        }
    }
}




